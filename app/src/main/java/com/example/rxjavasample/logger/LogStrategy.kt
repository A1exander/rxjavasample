package com.example.rxjavasample.logger

/**
 * Используется для разграничения стратегий логирования по разным темам
 * Типы Observable
 * Типы Subject
 * Типы Operators
 */
sealed class LogStrategy : StrategyExecutor {

    // Observable types start
    abstract class ObservableStrategy(
        override val strategyName: String = "Simple observable type"
    ) : LogStrategy()

    abstract class MaybeStrategy(
        override val strategyName: String = "Maybe type"
    ) : LogStrategy()

    abstract class CompletableStrategy(
        override val strategyName: String = "Completable type"
    ) : LogStrategy()

    abstract class SingleStrategy(
        override val strategyName: String = "Single type"
    ) : LogStrategy()
    // Observable types end

    // Subject types start
    abstract class PublishSubjectStrategy(
        override val strategyName: String = "Publish subject type"
    ) : LogStrategy()

    abstract class BehaviorSubjectStrategy(
        override val strategyName: String = "Behavior subject type"
    ) : LogStrategy()

    abstract class ReplaySubjectStrategy(
        override val strategyName: String = "Replay subject type"
    ) : LogStrategy()

    abstract class AsyncSubjectStrategy(
        override val strategyName: String = "Async subject type"
    ) : LogStrategy()

    abstract class UnicastSubjectStrategy(
        override val strategyName: String = "Unicast subject type"
    ) : LogStrategy()
    // Subject types end

    // Operators types start
    abstract class MapOperatorStrategy(
        override val strategyName: String = "Map operator type"
    ) : LogStrategy()

    abstract class FilterOperatorStrategy(
        override val strategyName: String = "Filter operator type"
    ) : LogStrategy()

    abstract class FirstOperatorStrategy(
        override val strategyName: String = "First operator type"
    ) : LogStrategy()

    abstract class IgnoreElementsOperatorStrategy(
        override val strategyName: String = "IgnoreElements operator type"
    ) : LogStrategy()

    abstract class MergeOperatorStrategy(
        override val strategyName: String = "Merge operator type"
    ) : LogStrategy()

    abstract class ConcatOperatorStrategy(
        override val strategyName: String = "Concat operator type"
    ) : LogStrategy()

    abstract class DelayOperatorStrategy(
        override val strategyName: String = "Delay operator type"
    ) : LogStrategy()

    abstract class DebounceOperatorStrategy(
        override val strategyName: String = "Debounce operator type"
    ) : LogStrategy()

    abstract class SkipOperatorStrategy(
        override val strategyName: String = "Skip operator type"
    ) : LogStrategy()

    abstract class TakeOperatorStrategy(
        override val strategyName: String = "Take operator type"
    ) : LogStrategy()

    abstract class ZipOperatorStrategy(
        override val strategyName: String = "Zip operator type"
    ) : LogStrategy()

    abstract class SkipWhileOperatorStrategy(
        override val strategyName: String = "SkipWhile operator type"
    ) : LogStrategy()

    abstract class TakeWhileOperatorStrategy(
        override val strategyName: String = "TakeWhile operator type"
    ) : LogStrategy()

    abstract class FlatMapOperatorStrategy(
        override val strategyName: String = "FlatMap operator type"
    ) : LogStrategy()

    abstract class ConcatMapOperatorStrategy(
        override val strategyName: String = "ConcatMap operator type"
    ) : LogStrategy()
    // Operators types start

    // subscribeOn()/observeOn() operator types start
    abstract class SubscribeOnAndObserveOnOperatorStrategy(
        override val strategyName: String = "SubscribeOnAndObserveOn operator type"
    ) : LogStrategy()
    // subscribeOn()/observeOn() operator types end

}