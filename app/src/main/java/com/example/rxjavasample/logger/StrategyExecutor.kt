package com.example.rxjavasample.logger

interface StrategyExecutor {

    val strategyName: String

    fun execute()

}