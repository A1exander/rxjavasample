package com.example.rxjavasample.logger

import com.example.rxjavasample.utils.logAnchor

/**
 * Запускает нужный класс с примером
 */
fun runWithLogStrategy(logStrategy: LogStrategy) {
    logAnchor("Start", logStrategy)

    logStrategy.execute()

    logAnchor("End", logStrategy)
}