package com.example.rxjavasample

import com.example.rxjavasample.logger.runWithLogStrategy
import com.example.rxjavasample.observabletypes.observable.SuccessObservable

/**
 * Точка входа в программу
 *
 * Для запуска определенного сценария логирования, требуется передать в метод runWithLogStrategy(),
 * соответствующую стратегию запуска
 *
 * Возможные типы стратегий приведены ниже
 *
 * Источники данных:
 * Observable - SuccessObservable(), ErrorObservable()
 * Single - SuccessSingle(), ErrorSingle()
 * Completable - ErrorCompletable(), SuccessCompletable()
 * Maybe - CompleteMaybe(), ErrorMaybe(), SuccessMaybe()
 *
 * Subjects:
 * PublishSubject - PublishSubjectSample()
 * BehaviorSubject - BehaviorSubjectSample()
 * ReplaySubject - ReplaySubjectSample()
 * AsyncSubject - AsyncSubjectSample()
 * UnicastSubject - UnicastSubjectSample()
 *
 * Операторы:
 * Операторы фильтрации - FilterOperator(), FirstOperator(), IgnoreElementsOperator(), SkipOperator(), SkipWhileOperator()
 * TakeOperator(), TakeWhileOperator()
 *
 * Операторы объединения - ConcatOperator(), MergeOperator(), ZipOperator()
 *
 * Операторы разложения и преобразования - ConcatMapOperator(), FlatMapOperator(), MapOperator()
 *
 * Операторы с временным интервалом - DebounceOperator(), DelayOperator()
 *
 * Оператор управления потоками - SubscribeOnAndObserveOnOperator()
 */
fun main() {
    runWithLogStrategy(SuccessObservable())
}