package com.example.rxjavasample.observabletypes.maybe

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.Maybe

class SuccessMaybe : LogStrategy.MaybeStrategy() {

    /**
     * Пример Maybe с нормальным завершением работы, результатом которой является какой-то элемент
     */
    override fun execute() {
        // создаем поток данных в виде Maybe<Int>, с одним значением
        val maybe = Maybe.just(1)

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = maybe
            .subscribe(
                /* данный метод отработает и будет терминальным для данной последовательности
                * (последовательность завершилась с успехом, результатом работы является полу-
                * ченное значение) */
                ::logOnSuccess,
                // данный коллбек не отработает
                ::logOnError,
                // данный коллбек не отработает
                ::logOnComplete
            )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}