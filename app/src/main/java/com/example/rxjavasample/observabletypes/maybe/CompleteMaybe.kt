package com.example.rxjavasample.observabletypes.maybe

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.Maybe

class CompleteMaybe : LogStrategy.MaybeStrategy() {

    /**
     * Пример Maybe нормально завершаемого без испускаемого значения
     *
     * В данном случае, отработает только коллбек onComplete()
     */
    override fun execute() {
        // создаем поток данных в виде Maybe<Unit>, без каких-либо значений
        val maybe = Maybe.empty<Unit>()

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = maybe
            .subscribe(
                // коллбек не отработает
                ::logOnSuccess,
                // коллбек не отработает
                ::logOnError,
                /* т.к. последовательность была выполнена без каких-либо
                * элементов в ней, выполняется данный коллбек */
                ::logOnComplete
            )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}