package com.example.rxjavasample.observabletypes.completable

import com.example.rxjavasample.exception.CustomRxException
import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.logDispose
import com.example.rxjavasample.utils.logOnComplete
import com.example.rxjavasample.utils.logOnError
import com.example.rxjavasample.utils.logSubscription
import io.reactivex.Completable

class ErrorCompletable : LogStrategy.CompletableStrategy() {

    /**
     * Нормальное поведение Completable, отправляющей ошибку
     *
     * Данный источник данных может завершаться только успешно или только с ошибкой
     */
    override fun execute() {
        // создаем поток данных в виде Completable
        val completable = Completable.create { emitter ->
            // отправляем в поток данных ошибку
            emitter.onError(CustomRxException())
        }

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = completable.subscribe(
            // коллбек не отработает
            ::logOnComplete,
            // перехватываем ошибку и выводим соответствующую информацию в лог
            ::logOnError
        )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}