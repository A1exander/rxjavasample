package com.example.rxjavasample.observabletypes.single

import com.example.rxjavasample.exception.CustomRxException
import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.logDispose
import com.example.rxjavasample.utils.logOnError
import com.example.rxjavasample.utils.logOnSuccess
import com.example.rxjavasample.utils.logSubscription
import io.reactivex.Single

class ErrorSingle : LogStrategy.SingleStrategy() {

    /**
     * Пример поведения Single при возникновении ошибки
     *
     * В данном случае, за объяснением по отправке ошибки, следует обратиться к
     * @see com.example.rxjavasample.observabletypes.maybe.ErrorMaybe#execute()
     */
    override fun execute() {
        // создаем поток данных в виде Single<Int> и отправляем в него ошибку
        val single = Single.error<Int>(CustomRxException())

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = single.subscribe(
            // коллбек не отработает
            ::logOnSuccess,
            // перехватит ошибку и выведет в лог
            ::logOnError
        )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}