package com.example.rxjavasample.observabletypes.single

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.logDispose
import com.example.rxjavasample.utils.logOnError
import com.example.rxjavasample.utils.logOnSuccess
import com.example.rxjavasample.utils.logSubscription
import io.reactivex.Single

class SuccessSingle : LogStrategy.SingleStrategy() {

    /**
     * Пример нормального поведения Single
     *
     * Данный тип источника данных похож на Maybe, за тем исключением, что в Maybe присутствует коллбэк
     * onComplete(), в результате, источник Maybe может быть успешно завершен со значением, либо без него.
     * В Single, нормальное завершение работы поддерживается только с каким-то одним элементом
     */
    override fun execute() {
        // создаем поток данных в виде Single<Int>, который отправит только одно значение
        // (Single же)
        val single = Single.just(1)

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = single.subscribe(
            /* отработает данный метод и получит наш отправленный элемент, после чего,
            * последовательность завершится */
            ::logOnSuccess,
            // не отработает
            ::logOnError
        )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}