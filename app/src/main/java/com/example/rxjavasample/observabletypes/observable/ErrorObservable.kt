package com.example.rxjavasample.observabletypes.observable

import com.example.rxjavasample.exception.CustomRxException
import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*

class ErrorObservable : LogStrategy.ObservableStrategy() {

    /**
     * Пример Observable с возникшей ошибкой
     *
     * В данном случае, будет получен первый элемент потока данных, после чего, будет выброшено исключение
     * CustomRxException, которое провалится в коллбек onError()
     */
    override fun execute() {
        // создаем поток данных в виде Observable<Int>
        val observable = observableNumbers

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = observable.subscribe(
            { item ->
                /* логируем полученные элементы последовательности(в данном случае,
                * мы получим только первый элемент потока данных) */
                logOnNext(item)
                // эмитируем возникновение ошибки при обработке элементов последовательности
                throw CustomRxException()
            },
            // ошибка будет отловлена данным коллбеком
            ::logOnError,
            // а этот не отработает
            ::logOnComplete
        )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}