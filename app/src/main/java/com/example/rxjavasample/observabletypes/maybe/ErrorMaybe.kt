package com.example.rxjavasample.observabletypes.maybe

import com.example.rxjavasample.exception.CustomRxException
import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.Maybe

class ErrorMaybe : LogStrategy.MaybeStrategy() {

    /**
     * Пример источника данных Maybe с ошибкой
     *
     * Из-за различий с тем же Observable, у которого есть onNext(onError|onComplete),
     * возникновение ошибки в коллбеке onNext прокинется в onError. У Maybe при прокидывании ошибки
     * в коллбеке onSuccess() будет выдано другое исключение UndeliverableException(), в следствие чего краш
     * всего приложения. Поэтому ошибка прокидывается из емиттера.
     */
    override fun execute() {
        /* .create { emitter -> ... } - еще один из вариантов, как можно создать поток данных(Observable, Maybe, ...),
         * с тем различием, что тут пользователь явно может контролировать, что конкретно нужно отправить
         * в последовательность
         *
         * создаем поток данных в виде Maybe<Int> и отправляем в поток данных ошибку*/
        val maybe = Maybe.error<Int>(CustomRxException())

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = maybe
            .subscribe(
                // коллбек не отработает
                ::logOnSuccess,
                // перехватываем ошибку в данном коллбеке
                ::logOnError,
                // коллбек не отработает
                ::logOnComplete
            )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}