package com.example.rxjavasample.observabletypes.completable

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.logDispose
import com.example.rxjavasample.utils.logOnComplete
import com.example.rxjavasample.utils.logOnError
import com.example.rxjavasample.utils.logSubscription
import io.reactivex.Completable

class SuccessCompletable : LogStrategy.CompletableStrategy() {

    /**
     * Поведение Completable с нормальным завершение потока данных
     *
     * Данный источник данных может завершаться только успешно или только с ошибкой
     */
    override fun execute() {
        // создаем поток данных в виде Completable
        val completable = Completable.create { emitter ->
            // отправляем в поток данных ивент Complete
            emitter.onComplete()
        }

        logSubscription()
        /* создаем конкретную подписку и передаем необходимые методы
        * для получения коллбеков */
        val subscription = completable.subscribe(
            // коллбек отработает и выведет соответствующую информацию в лог
            ::logOnComplete,
            // коллбек не отработает
            ::logOnError
        )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}