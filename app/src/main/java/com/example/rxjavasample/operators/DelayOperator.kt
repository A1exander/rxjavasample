package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import java.util.concurrent.TimeUnit

class DelayOperator : LogStrategy.DelayOperatorStrategy() {

    /**
     * Оператор delay позволяет "сдвинуть" отправку данных на какое-то время
     * в качестве параметров, данный оператор принимает длину задержки и
     * единицу времени данной задержки(миллисекунды, секунды, минуты, часы ...)
     *
     * Есть несколько перегрузок данного метода, одна из которых используется в этом примере
     * Данная перегрузка автоматически выполняется в Schedulers.computation(), поэтому
     * нам необходимо дождаться результата выполнения задержки(Thread.currentThread().join(1100))
     * Так же, есть перегузка метода, в которой можно явно указать нужный Шедудер
     */
    override fun execute() {
        // создаем поток данных в виде Observable<Int>
        val observable = observableNumbers

        logSubscription()
        // возьмем за точку отсчета текущее время системы
        val startTime = System.currentTimeMillis()
        val subscription = observable
            /* добавляем оператор delay, выставляем длину задержки 1 и единицу времени секунду,
            * каждый выпущенный источником данных элемент, будет принят подписчиком с секундной задержкой */
            .delay(1, TimeUnit.SECONDS)
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем поток данных после задержки
                { item ->
                    // высчитываем сколько времени прошло, после отправки данного элемента
                    val receiveTime = System.currentTimeMillis() - startTime
                    // выводим в лог, время ожидания каждого элемента
                    println("Receive time: $receiveTime")
                    logOnNext(item)
                },
                // коллбек не выполнится
                ::logOnError,
                // выполнится после окончания потока данных
                ::logOnComplete
            )

        // ожидаем, пока источник данных, отправит все элементы
        Thread.currentThread().join(1100)
        // отписываемся от источника данных, дабы не было утечки
        subscription.logDispose()
    }

}