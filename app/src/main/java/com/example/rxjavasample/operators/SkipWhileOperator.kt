package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*

class SkipWhileOperator : LogStrategy.SkipWhileOperatorStrategy() {

    /**
     * Оператор skipWhile позволяет пропустить некоторые элементы последовательности данных,
     * определяемые функцией предикатом
     *
     * Пока функция-предикат возвращает true - элементы будут пропущены, после
     * результата false - все последующие элементы будут отправлены подписчику
     */
    override fun execute() {
        // создаем поток данных в виде Observable<Int>
        val observable = observableNumbers

        logSubscription()
        val subscription = observable
            // добавляем оператор skipWhile с функцией-предикатом
            .skipWhile { item ->
                item != 4
            }
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем измененный поток данных
                ::logOnNext,
                // коллбек не выполнится
                ::logOnError,
                // выполнится после окончания потока данных
                ::logOnComplete
            )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}