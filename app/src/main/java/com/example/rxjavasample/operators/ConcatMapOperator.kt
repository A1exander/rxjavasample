package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class ConcatMapOperator : LogStrategy.ConcatMapOperatorStrategy() {

    /**
     * Оператор concatMap, похож на
     * @see com.example.rxjavasample.operators.FlatMapOperator
     * за исключением того, что данный оператор сохраняет последовательность элементов из внутренних источников данных
     *
     * Результатом такого слияния будет - принятые подписчиком все элементы первого источника данных
     * (в порядке их отправки), после чего, будут получены все элементы второго источника данных(в
     * порядке их отправки) и т.д.
     */
    override fun execute() {
        // создаем первый поток данных в виде Observable<Int>
        val firstObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(1)
            Thread.sleep(100)
            emitter.onNext(2)
            Thread.sleep(100)
            emitter.onNext(3)
            Thread.sleep(100)
            emitter.onNext(4)
            Thread.sleep(100)
            emitter.onNext(5)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем второй поток данных в виде Observable<Int>
        val secondObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(6)
            Thread.sleep(100)
            emitter.onNext(7)
            Thread.sleep(100)
            emitter.onNext(8)
            Thread.sleep(100)
            emitter.onNext(9)
            Thread.sleep(100)
            emitter.onNext(0)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем третий поток данных в виде Observable<Int>
        val thirdObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(11)
            Thread.sleep(100)
            emitter.onNext(12)
            Thread.sleep(100)
            emitter.onNext(13)
            Thread.sleep(100)
            emitter.onNext(14)
            Thread.sleep(100)
            emitter.onNext(15)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем поток данных в виде Observable<Observable<Int>>
        val observableOfObservables = Observable.just(
            firstObservable
                /* даннный оператор рассматриваеся в
                @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                * Источник данных начинает испускать элементы в IO пуле потоков */
                .subscribeOn(Schedulers.io()),
            secondObservable
                /* даннный оператор рассматриваеся в
                @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                * Источник данных начинает испускать элементы в новом потоке */
                .subscribeOn(Schedulers.newThread()),
            thirdObservable
                /* даннный оператор рассматриваеся в
                @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                * Источник данных начинает испускать элементы в потоках для сложных операций */
                .subscribeOn(Schedulers.computation())
        )

        logSubscription()
        val subscription = observableOfObservables
            /* даннный оператор рассматриваеся в
            @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
            * Источник данных начинает испускать элементы в IO пуле потоков */
            .subscribeOn(Schedulers.io())
            /* объединяем потоки данных, последовательность испущенных элементов в результирующем потоке
            * данных будет сохранена */
            .concatMap { observable ->
                observable.map { item ->
                    item.toDouble()
                }
                // можно использовать так, как написано ниже, в этом случае, результирующий поток данных будет Observable<Int>
                // currentObservable
            }
            /* даннный оператор рассматриваеся в
            @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
            * обработка данных осуществляется в текущем потоке приложения */
            .observeOn(Schedulers.trampoline())
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем данные от двух объединенных источников данных
                ::logOnNext,
                // коллбек не выполнится
                ::logOnError,
                // логируем успешное завершение результирующей последовательности
                ::logOnComplete
            )

        // ожидаем, пока источники данных в соответствующих потоках, отправят все элементы
        Thread.currentThread().join(2500)
        // отписываемся от источника данных, дабы не было утечки
        subscription.logDispose()
    }

}