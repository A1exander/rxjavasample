package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*

class FirstOperator : LogStrategy.FirstOperatorStrategy() {

    /**
     * Оператор first фактически делает из последовательности данных
     * источник Single с одним элементом
     *
     * При вызове данного опреатора, будет взят только первый элемент потока данных,
     * а при отсутствии элементов в последовательности, значение, которое было передано
     * в качестве параметра этого оператора
     */
    override fun execute() {
        // создаем поток данных в виде Observable<Int>
        val observable = observableNumbers

        logSubscription()
        val subscription = observable
            /* добавляем оператор first и значение, которое будет отправлено подписчикам,
            * если последовательность окажется пустой */
            .first(0)
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                /* т.к. у нас получается источник данных Single, его метод, со значением -
                * это onSuccess(item: T) */
                ::logOnSuccess,
                // коллбек не выполнится
                ::logOnError
            )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}