package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MergeOperator : LogStrategy.MergeOperatorStrategy() {

    /**
     * Данный оператор похож на
     * @see com.example.rxjavasample.operators.ConcatOperator
     *
     * Оператор mergeWith объединяет несколько источников данных так, что на выходе,
     * мы имеем дело с одним источником. При этом, данный оператор сохраняет последовательность
     * отправленных элементов последовательностей
     *
     * В данном примере приведено объединение двух последовательностей, но можно сделать и больше
     */
    override fun execute() {
        // создаем первый поток данных в виде Observable<Int>
        val firstObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(1)
            Thread.sleep(100)
            emitter.onNext(2)
            Thread.sleep(100)
            emitter.onNext(3)
            Thread.sleep(100)
            emitter.onNext(4)
            Thread.sleep(100)
            emitter.onNext(5)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем второй поток данных в виде Observable<Int>
        val secondObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элементов */
            emitter.onNext(6)
            Thread.sleep(100)
            emitter.onNext(7)
            Thread.sleep(100)
            emitter.onNext(8)
            Thread.sleep(100)
            emitter.onNext(9)
            Thread.sleep(100)
            emitter.onNext(0)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        logSubscription()
        val subscription = firstObservable
            /* даннный оператор рассматриваеся в
            @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
            * Источник данных начинает испускать элементы в IO пуле потоков */
            .subscribeOn(Schedulers.io())
            // объединяем потоки данных, сохраняя очередность выпущенных элементов в результирующей Observable
            .mergeWith(
                secondObservable
                    /* даннный оператор рассматриваеся в
                    @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                    * Источник данных начинает испускать элементы в новом потоке */
                    .subscribeOn(Schedulers.newThread())
            )
            /* даннный оператор рассматриваеся в
            @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
            * обработка данных осуществляется в текущем потоке приложения */
            .observeOn(Schedulers.trampoline())
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем данные от двух объединенных источников данных
                ::logOnNext,
                // коллбек не выполнится
                ::logOnError,
                // логируем успешное завершение результирующей последовательности
                ::logOnComplete
            )

        // ожидаем, пока источники данных в соответствующих потоках, отправят все элементы
        Thread.currentThread().join(1000)
        // отписываемся от источника данных, дабы не было утечки
        subscription.logDispose()
    }

}