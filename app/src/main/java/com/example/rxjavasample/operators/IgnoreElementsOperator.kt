package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*

class IgnoreElementsOperator : LogStrategy.IgnoreElementsOperatorStrategy() {

    /**
     * Оператор ignoreElements фактически делает из последовательности данных
     * источник Completable
     *
     * При вызове данного опреатора, будут проигнорированы все отправленные источником данных
     * элементы, но будет отправлен терминальный ивент onSuccess() или onError()
     */
    override fun execute() {
        // создаем поток данных в виде Observable<Int>
        val observable = observableNumbers

        logSubscription()
        val subscription = observable
            /* добавляем оператор first и значение, которое будет отправлено подписчикам,
            * если последовательность окажется пустой */
            .ignoreElements()
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // данный поток данных успешно завершается, поэтому отработает данный коллбек
                ::logOnComplete,
                // коллбек не выполнится
                ::logOnError
            )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}