package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.schedulers.Schedulers

class SubscribeOnAndObserveOnOperator : LogStrategy.SubscribeOnAndObserveOnOperatorStrategy() {

    /**
     * Операторы subscribeOn() и observeOn(), чаще всего применяются вместе
     *
     * Оператор subscribeOn() определяет в каком потоке(пуле потоков), будут отправляться элементы в поток данных
     * Применение нескольких subscribeOn() ничего не даст, будет срабатывать только первый такой оператор, остальные
     * будет игнорироваться
     *
     * Оператор observeOn() определяет в каком потоке(пуле потоков) будут наблюдаться/изменяться данные
     * Применение нескольких таких операторов, будет переключать поток выполнения
     *
     * Для определения потока(пула потоков), в RxJava есть Schedulers, со статическими методами для выбора
     * конкретного потока или пула
     *
     * Schedulers.io() - пул потоков, используемый для сетевых запросов, операций чтения/записи в файл/базу данных
     *
     * Schedulers.computation() - используется для тяжелых вычислений, создает пул потоков, количеством, соответствующим
     * числу ядер в процессоре
     *
     * Schedulers.trampoline() - выполняет код в текущем потоке
     *
     * Schedulers.newThread() - создает новый поток для каждого активного Observable
     *
     * Schedulers.single() - выполняет всю работу последовательно в одном потоке
     *
     * AndroidSchedulers.mainThread() - данного Шедулера нет в библиотеке RxJava, он является частью RxAndroid
     * и позволяет наблюдать/изменять данные в main-потоке приложения
     *
     * Данный пример можно расценивать как небольшую Rx-цепочку
     */
    override fun execute() {
        // создаем поток данных в виде Observable<Int>
        val observable = observableNumbers

        logSubscription()
        val subscription = observable
            // все элементы потока данных, будут испускаться в IO пуле потоков
            .subscribeOn(Schedulers.io())
            /* добавляем оператор map
            * в данном случае, если перед другим оператором или подпиской нет observeOn(), тогда выполнение работы
            * будет происходить в потоке(пуле потоков), определенным subscribeOn() - в данном случае - IO пуле */
            .map { item ->
                // проверяем в каком потоке мы будем маппить данные
                logCurrentThreadName()
                // трансформируем входные целые числа в строки и дальше в Rx-цепочку
                logMapOperator(item, "String")
                item.toString()
            }
            // меняем пул потоков, для выполнения нашей сложной задачи
            .observeOn(Schedulers.computation())
            /* добавляем еще один оператор map
            * он уже будет выполняться в пуле computation */
            .map {
                // проверяем в каком потоке мы будем маппить данные
                logCurrentThreadName()
                // трансформируем входные строки в дробные числа и отдаем подписчику
                logMapOperator(it, "Double")
                it.toDouble()
            }
            // еще раз меняем поток выполнения
            .observeOn(Schedulers.newThread())
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем измененный поток данных
                { item ->
                    // смотрим в каком потоке происходит сбор данных
                    logCurrentThreadName()
                    logOnNext(item)
                },
                // коллбек не выполнится
                ::logOnError,
                // выполнится после окончания потока данных
                ::logOnComplete
            )

        // ожидаем, пока источники данных в соответствующих потоках, отправят все элементы
        Thread.currentThread().join(300)
        // отписываемся от источника данных, дабы не было утечки
        subscription.logDispose()
    }

}