package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class ZipOperator : LogStrategy.ZipOperatorStrategy() {

    /**
     * Оператор zipWith объединяет элементы из нескольких потоков данных в один соответствующий элемент
     * Данный опреатор имеет строгую очередность, где каждый элемент первой последовательности, соответствует
     * элементу во второй(третьей) последовательности
     *
     * В качестве параметра принимает другую последовательность данных и функцию-маппер, которая
     * трансформирует элементы из разных потоков данных в одиин общий элемент
     *
     * Количество элементов, в результирующей последовательности данных определяется количеством элементов
     * в наименьшем потоке данных
     */
    override fun execute() {
        // создаем первый поток данных в виде Observable<Int>
        val firstObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(1)
            emitter.onNext(2)
            emitter.onNext(3)
            emitter.onNext(4)
            emitter.onNext(5)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем второй поток данных в виде Observable<Int>
        val secondObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элементов */
            emitter.onNext(6)
            Thread.sleep(100)
            emitter.onNext(7)
            Thread.sleep(100)
            emitter.onNext(8)
            Thread.sleep(100)
            emitter.onNext(9)
            Thread.sleep(100)
            emitter.onNext(0)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        logSubscription()
        val subscription = firstObservable
            /* даннный оператор рассматриваеся в
            @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
            * Источник данных начинает испускать элементы в IO пуле потоков */
            .subscribeOn(Schedulers.io())
            // объединяет элементы из двух потоков данных, в один элемент и отправляет его подписчику
            .zipWith(
                secondObservable
                    /* даннный оператор рассматриваеся в
                    @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                    * Источник данных начинает испускать элементы в новом потоке */
                    .subscribeOn(Schedulers.newThread())
            ) // функция для нужного преобразования элементов их двух потоков данных в один требуемый элемент
            { firstItem, secondItem -> "$firstItem -> $secondItem" }
            /* даннный оператор рассматриваеся в
            @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
            * обработка данных осуществляется в текущем потоке приложения */
            .observeOn(Schedulers.trampoline())
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем данные от двух объединенных источников данных
                ::logOnNext,
                // коллбек не выполнится
                ::logOnError,
                // логируем успешное завершение результирующей последовательности
                ::logOnComplete
            )

        // ожидаем, пока источники данных в соответствующих потоках, отправят все элементы
        Thread.currentThread().join(1000)
        // отписываемся от источника данных, дабы не было утечки
        subscription.logDispose()
    }

}