package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class FlatMapOperator : LogStrategy.FlatMapOperatorStrategy() {

    /**
     * Оператор flatMap позволяет разложить поток данных вида Observable<Observable<T>> до вида
     * Observable<T>
     *
     * Имея источник данных, элементами которого являются также источники данных, с помощью данного
     * оператора, мы раскладываем элементы всех последовательностей в одну результирующую
     * Так же в этот момент, мы можем трансформировать каждый элемент последовательности
     *
     * Элементы в результирующей последовательности могут находиться в любом порядке
     * Для сохранения порядка отправляемы элементов, можно воспользоваться
     * @see com.example.rxjavasample.operators.ConcatMapOperator
     */
    override fun execute() {
        // создаем первый поток данных в виде Observable<Int>
        val firstObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(1)
            Thread.sleep(100)
            emitter.onNext(2)
            Thread.sleep(100)
            emitter.onNext(3)
            Thread.sleep(100)
            emitter.onNext(4)
            Thread.sleep(100)
            emitter.onNext(5)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем второй поток данных в виде Observable<Int>
        val secondObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(6)
            Thread.sleep(100)
            emitter.onNext(7)
            Thread.sleep(100)
            emitter.onNext(8)
            Thread.sleep(100)
            emitter.onNext(9)
            Thread.sleep(100)
            emitter.onNext(0)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем третий поток данных в виде Observable<Int>
        val thirdObservable = Observable.create<Int> { emitter ->
            /* отправляем данные подписчикам и приостанавливаем текущий поток, для эмулирования
            * задержки в получении новых элеиентов */
            emitter.onNext(11)
            Thread.sleep(100)
            emitter.onNext(12)
            Thread.sleep(100)
            emitter.onNext(13)
            Thread.sleep(100)
            emitter.onNext(14)
            Thread.sleep(100)
            emitter.onNext(15)
            Thread.sleep(100)

            // после отправки всех элементов, завершаем последовательность
            emitter.onComplete()
        }

        // создаем поток данных в виде Observable<Observable<Int>>
        val observableOfObservables = Observable.just(
            firstObservable
                /* даннный оператор рассматриваеся в
                @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                * Источник данных начинает испускать элементы в IO пуле потоков */
                .subscribeOn(Schedulers.io()),
            secondObservable
                /* даннный оператор рассматриваеся в
                @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                * Источник данных начинает испускать элементы в новом потоке */
                .subscribeOn(Schedulers.newThread()),
            thirdObservable
                /* даннный оператор рассматриваеся в
                @see com.example.rxjavasample.operators.SubscribeOnAndObserveOnOperator
                * Источник данных начинает испускать элементы в потоках для сложных операций */
                .subscribeOn(Schedulers.computation())
        )

        logSubscription()
        val subscription = observableOfObservables
            /* добавляем оператор flatMap,
            * парметром которого является функция, к ней на вход поступает определенный источник данных, с
            * которым мы можем работать как обычно, в том числе применять какие-либо операторы */
            .flatMap { currentObservable ->
                // попутно трансформируем каждый элемент в Double(шаг не обязательный, просто для показа, что можно еще и маппить)
                currentObservable.map { item ->
                    item.toDouble()
                }
                // можно использовать так, как написано ниже, в этом случае, результирующий поток данных будет Observable<Int>
                // currentObservable
            }
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем измененный поток данных
                ::logOnNext,
                // коллбек не выполнится
                ::logOnError,
                // выполнится после окончания потока данных
                ::logOnComplete
            )

        // ожидаем, пока источники данных в соответствующих потоках, отправят все элементы
        Thread.currentThread().join(1000)
        // отписываемся от источника данных, дабы не было утечки
        subscription.logDispose()
    }

}