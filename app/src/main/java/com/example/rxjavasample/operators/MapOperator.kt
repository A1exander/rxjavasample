package com.example.rxjavasample.operators

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*

class MapOperator : LogStrategy.MapOperatorStrategy() {

    /**
     * Оператор map позволяет преобразоватть тип элементов в последоваельности
     * и продолжить работу так, как будто никакого оператора и не было
     *
     * map получает на вход данные одного типа, а на выход может передать данные
     * совершенно другого. При этом, работа подписчика никак не изменится
     */
    override fun execute() {
        // создаем поток данных в виде Observable<Int>
        val observable = observableNumbers

        logSubscription()
        val subscription = observable
            // добавляем оператор map
            .map { item ->
                // трансформируем входные целые числа в строки и отдаем подписчику
                logMapOperator(item, "String")
                item.toString()
            }
            /* создаем конкретную подписку и передаем необходимые методы
            * для получения коллбеков */
            .subscribe(
                // получаем измененный поток данных
                ::logOnNext,
                // коллбек не выполнится
                ::logOnError,
                // выполнится после окончания потока данных
                ::logOnComplete
            )

        /* отписываемся от источника данных, дабы не было утечки
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        subscription.logDispose()
    }

}