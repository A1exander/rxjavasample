package com.example.rxjavasample.utils

import io.reactivex.Observable

/**
 * Вместо fromIterable() можно использовать just(1, 2, ... 9)
 */
val observableNumbers = Observable.fromIterable(1..9)