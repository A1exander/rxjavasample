package com.example.rxjavasample.utils

import com.example.rxjavasample.logger.LogStrategy

fun <T> logOnNext(item: T) {
    println("New item collected: $item")
}

fun <T> logSubjectOnNext(item: T, observerName: String) {
    println("New item collected: $item, by: $observerName")
}

fun logOnError(error: Throwable) {
    println("\nAn error occurred: $error")
}

fun logSubjectOnError(error: Throwable, observerName: String) {
    println("\nAn error occurred: $error, for: $observerName\n")
}

fun logOnComplete() {
    println("Observable successfully completed\n")
}

fun <T> logOnSuccess(item: T) {
    println("\nObservable successfully ended with item: $item")
}

fun logSubscription() {
    println("Create a new subscription to current observable\n")
}

fun <T> logMapOperator(item: T, type: String) {
    println("Transforming item: $item, to type: $type")
}

fun logCurrentThreadName() {
    println("\nExecution on: ${Thread.currentThread().name} thread")
}

fun logAnchor(anchor: String, logStrategy: LogStrategy) {
    println(
        "\n--------------- $anchor of the ${logStrategy.strategyName} ---------------\n"
    )
}