package com.example.rxjavasample.utils

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Disposable - возвращаемая сущность, при совершении подписки на Observable, Maybe, ...
 *
 * Позволяет после завершения потока данных отписаться от производителя данных, дабы избежать утечки
 */
fun Disposable.logDispose() {
    dispose()
    println("\nSubscription is disposed")
}

/**
 * Функция используется для добавления текущей подписки в конкретную CompositeDisposable
 * @param compositeDisposable нужный держатель подписок, который впоследсвие будет очищен
 */
fun Disposable.addTo(compositeDisposable: CompositeDisposable) {
    compositeDisposable.add(this)
}