package com.example.rxjavasample.utils

import io.reactivex.disposables.CompositeDisposable

/**
 * Очищение подписок и вывод в лог соответствующей информации об этом
 *
 * У CompositeDisposable есть 2 метода для очистки подписок:
 * clear() и dispose(), их разница в том, что при вызове метода clear(), очистятся текущие подписки,
 * но в будущем, в данную CompositeDisposable будет возможно добавить новые подписки
 * При вызове метода dispose(), добавить новые подписки будет невозможно
 *
 * Пример использования:
 * Во фрагменте, при вызове onDestroyView(), следует использовать clear(), т.к. фрагмент может пересоздать view
 * и заново сделать какие-то подписки(переиспользование созданной CompositeDisposable)
 * В активити, при вызове метода onDestroy(), лучше вызвать dispose(), т.к. после вызова этого метода,
 * переиспользоваться данная CompositeDisposable уже гарантированно не будет
 */
fun CompositeDisposable.logDispose() {
    dispose()
    println("\nAll subscriptions is disposed")
}