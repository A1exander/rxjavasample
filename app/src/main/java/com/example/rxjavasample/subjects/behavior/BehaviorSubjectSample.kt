package com.example.rxjavasample.subjects.behavior

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

class BehaviorSubjectSample : LogStrategy.BehaviorSubjectStrategy() {

    /**
     * Данный Subject похож на PublishSubject, посмотреть можно тут
     * @see com.example.rxjavasample.subjects.behavior.BehaviorSubjectSample
     *
     * За исключением того, что для новых подписчиков, будет воспроизведено последнее, отправленное перед
     * их подпиской значение
     *
     * Пример полностью повторяет NormalPublishSubject, за исключением того, что а качестве Subject,
     * используется BehaviorSubject
     */
    override fun execute() {
        /* CompositeDisposable используется для последующей очистки сразу нескольких Disposable
        * функция-расширение addTo() добавляет в переменную compositeDisposable текущую подписку */
        val compositeDisposable = CompositeDisposable()
        val behaviorSubject = BehaviorSubject.create<Int>()

        // эти данные не получит ни один подписчик, они выпущены вникуда
        behaviorSubject.onNext(1)
        behaviorSubject.onNext(2)

        // этот элемент получит первый подписчик(отправлен последним перед его подпиской)
        behaviorSubject.onNext(3)

        behaviorSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "First subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        /* при вызове onComplete(), все подписчики(даже будущие), получат коллбек onComplete()
        * если до вызова данного метода были выпущены какие-либо элементы и были подписаны новые
        * наблюдатели, они получат последние значения */
        // behaviorSubject.onComplete()

        // эти данные получит только первый подписчик, т.к. они были отправлены только после его подписки
        behaviorSubject.onNext(4)
        behaviorSubject.onNext(5)

        // этот элемент получит первый и второй подписчик(отправлен последний перед его подпиской)
        behaviorSubject.onNext(6)

        behaviorSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Second subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // эти данные получит первый и второй подписчики
        behaviorSubject.onNext(7)
        behaviorSubject.onNext(8)

        // а этот уже получат все подписчики
        behaviorSubject.onNext(9)

        behaviorSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Third subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // этот элемент получат все подписчики
        behaviorSubject.onNext(10)

        /* метод-расширение логирует и очищает все подписки, хранящиеся в данной CompositeDisposable
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        compositeDisposable.logDispose()
    }

}