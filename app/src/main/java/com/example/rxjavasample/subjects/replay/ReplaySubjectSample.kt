package com.example.rxjavasample.subjects.replay

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.ReplaySubject

class ReplaySubjectSample : LogStrategy.ReplaySubjectStrategy() {

    /**
     * Данный Subject имеет неограниченный буфер, для воспроизведения новым подписчикам всех
     * ранее выпущенных значений
     *
     * Имеется конструктор для задания размера буфера(но не указывает количество отправляемых элементов
     * новым подписчикам!). ReplaySubject имеет неограниченный буфер, представленный ArrayList. Конструктор,
     * позволяющий задать capacity, задает изначальный размер этого ArrayList'a
     *
     * Для создания ограниченного !буфера!, можно воспользоваться оператором .createWithSize(n)
     *
     * Пример полностью повторяет NormalPublishSubject, за исключением того, что а качестве Subject,
     * используется ReplaySubject
     */
    override fun execute() {
        /* CompositeDisposable используется для последующей очистки сразу нескольких Disposable
        * функция-расширение addTo() добавляет в переменную compositeDisposable текущую подписку */
        val compositeDisposable = CompositeDisposable()

        /* конструктор может принимать в себя размер буффера, соответствующего количеству воспроизводимых элементов
        * для уменьшения расходов при добавлении новых элементов

        * под капотом создается ArrayList(буфер воспроизводимых элементов), если заранее известно количество элементов,
        * для воспроизведения, следует указать размер буфера, тогда будет менее затратно добавлять новые элементы в буфер */
        val replaySubject = ReplaySubject.create<Int>()

        // будут получены всеми подписчиками
        replaySubject.onNext(1)
        replaySubject.onNext(2)
        replaySubject.onNext(3)

        replaySubject.subscribe(
            { item ->
                logSubjectOnNext(item, "First subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        /* при вызове onComplete(), все подписчики(даже будущие), получат коллбек onComplete(),
        * а также, все выпущенные до вызова onComplete() значения */
        // replaySubject.onComplete()

        // эти данные тоже получат все подписчики
        replaySubject.onNext(4)
        replaySubject.onNext(5)
        replaySubject.onNext(6)

        replaySubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Second subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // и это тоже все получат
        replaySubject.onNext(7)
        replaySubject.onNext(8)
        replaySubject.onNext(9)

        replaySubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Third subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // тоже получат все
        replaySubject.onNext(10)

        /* метод-расширение логирует и очищает все подписки, хранящиеся в данной CompositeDisposable
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        compositeDisposable.logDispose()
    }

}