package com.example.rxjavasample.subjects.async

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.AsyncSubject

class AsyncSubjectSample : LogStrategy.AsyncSubjectStrategy() {

    /**
     * Данный Subject отправляет подписчикам только !один! элемент, который был отправлен
     * непосредственно перед вызовом метода onComplete()(самый последний элемент в потоке данных)
     * Если же, до вызова onComplete() не было отправлено ни одного элемента, отрабатывать будет только
     * onComplete() коллбек
     *
     * Пример полностью повторяет NormalPublishSubject, за исключением того, что а качестве Subject,
     * используется AsyncSubject
     */
    override fun execute() {
        /* CompositeDisposable используется для последующей очистки сразу нескольких Disposable
        * функция-расширение addTo() добавляет в переменную compositeDisposable текущую подписку */
        val compositeDisposable = CompositeDisposable()
        val asyncSubject = AsyncSubject.create<Int>()

        // элементы не будут получены никем
        asyncSubject.onNext(1)
        asyncSubject.onNext(2)

        // будет получен только этот элемент, т.к. он последний отправляется в поток данных перед onComplete()
        asyncSubject.onNext(3)

        asyncSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "First subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        /* при вызове onComplete(), все подписчики(даже будущие), получат коллбек onComplete()
        * а также, последнее значение, отправленное в данный Subject */
        // asyncSubject.onComplete()

        // все подписчики получат элемент 3 и коллбек onComplete(), но больше ничего!
        asyncSubject.onComplete()

        // элементы не будут получены никем
        asyncSubject.onNext(4)
        asyncSubject.onNext(5)
        asyncSubject.onNext(6)

        asyncSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Second subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // элементы не будут получены никем
        asyncSubject.onNext(7)
        asyncSubject.onNext(8)
        asyncSubject.onNext(9)

        asyncSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Third subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // тоже никто не получит
        asyncSubject.onNext(10)

        /* метод-расширение логирует и очищает все подписки, хранящиеся в данной CompositeDisposable
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        compositeDisposable.logDispose()
    }

}