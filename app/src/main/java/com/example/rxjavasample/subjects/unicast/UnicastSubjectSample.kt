package com.example.rxjavasample.subjects.unicast

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.UnicastSubject

class UnicastSubjectSample : LogStrategy.UnicastSubjectStrategy() {

    /**
     * Данный Subject воспроизводит элементы, отправленные в поток данных только для одного подписчика,
     * того, кто первый подписался на данный Subject, всем остальным будет передано IllegalStateException
     * в коллбек onError()
     *
     * Пример полностью повторяет NormalPublishSubject, за исключением того, что а качестве Subject,
     * используется UnicastSubject
     */
    override fun execute() {
        /* CompositeDisposable используется для последующей очистки сразу нескольких Disposable
        * функция-расширение addTo() добавляет в переменную compositeDisposable текущую подписку */
        val compositeDisposable = CompositeDisposable()
        val unicastSubject = UnicastSubject.create<Int>()

        // будет получено только первым подписчиком
        unicastSubject.onNext(1)
        unicastSubject.onNext(2)
        unicastSubject.onNext(3)

        val firstSubscriberName = "First subscriber"
        unicastSubject.subscribe(
            { item ->
                logSubjectOnNext(item, firstSubscriberName)
            },
            { error ->
                logSubjectOnError(error, firstSubscriberName)
            },
            ::logOnComplete
        ).addTo(compositeDisposable)

        /* при вызове onComplete(), только первый подписчик получит ивент onComplete()
        * остальные так и продолжат получать соответствующий эксепшн */
        // unicastSubject.onComplete()

        // будет получено только первым подписчиком
        unicastSubject.onNext(4)
        unicastSubject.onNext(5)
        unicastSubject.onNext(6)

        val secondSubscriberName = "Second subscriber"
        unicastSubject.subscribe(
            { item ->
                logSubjectOnNext(item, secondSubscriberName)
            },
            { error ->
                logSubjectOnError(error, secondSubscriberName)
            },
            ::logOnComplete
        ).addTo(compositeDisposable)

        // будет получено только первым подписчиком
        unicastSubject.onNext(7)
        unicastSubject.onNext(8)
        unicastSubject.onNext(9)

        val thirdSubscriberName = "Third subscriber"
        unicastSubject.subscribe(
            { item ->
                logSubjectOnNext(item, thirdSubscriberName)
            },
            { error ->
                logSubjectOnError(error, thirdSubscriberName)
            },
            ::logOnComplete
        ).addTo(compositeDisposable)

        // будет получено только первым подписчиком
        unicastSubject.onNext(10)

        /* метод-расширение логирует и очищает все подписки, хранящиеся в данной CompositeDisposable
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        compositeDisposable.logDispose()
    }

}