package com.example.rxjavasample.subjects.publish

import com.example.rxjavasample.logger.LogStrategy
import com.example.rxjavasample.utils.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

class PublishSubjectSample : LogStrategy.PublishSubjectStrategy() {

    /**
     * Нормальное выполнение PublishSubject
     *
     * Subject - может выступать и в роли источника данных, и в роли подписчика.
     * Является горячим потоком данных - отправляет элементы независимо от того, есть ли подписчики или нет.
     * Холодный поток данных(Observable, Maybe, ...) - отправляют элементы только когда на них подписывается хоты бы
     * 1 подписчик. Новые подписчики получают только те значения, которые были отправлены после их подписки.
     *
     * PublishSubject - самый простой вид Subject
     */
    override fun execute() {
        /* CompositeDisposable используется для последующей очистки сразу нескольких Disposable
        * функция-расширение addTo() добавляет в переменную compositeDisposable текущую подписку */
        val compositeDisposable = CompositeDisposable()
        val publishSubject = PublishSubject.create<Int>()

        // эти данные не получит ни один подписчик, они выпущены вникуда
        publishSubject.onNext(1)
        publishSubject.onNext(2)
        publishSubject.onNext(3)

        publishSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "First subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // при вызове onComplete(), все подписчики(даже будущие), получат коллбек onComplete()
        // publishSubject.onComplete()

        // эти данные получит только первый подписчик, т.к. они были отправлены только после его подписки
        publishSubject.onNext(4)
        publishSubject.onNext(5)
        publishSubject.onNext(6)

        publishSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Second subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // эти данные получит первый и второй подписчики
        publishSubject.onNext(7)
        publishSubject.onNext(8)
        publishSubject.onNext(9)

        publishSubject.subscribe(
            { item ->
                logSubjectOnNext(item, "Third subscriber")
            },
            ::logOnError,
            ::logOnComplete
        ).addTo(compositeDisposable)

        // этот элемент получат все подписчики
        publishSubject.onNext(10)

        /* метод-расширение логирует и очищает все подписки, хранящиеся в данной CompositeDisposable
        * т.к. данный пример выполняется в однопоточной формате(без шедулеров и других потоков), данный код отработает
        * синхронно и отписка произойдет только после испускания всех элементов последовательности */
        compositeDisposable.logDispose()
    }

}